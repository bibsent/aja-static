Dette repoet inneholder statiske filer som trengs av andre prosjekter, og som publiseres på https://static.aja.bs.no/

Filene er synlige for verden, så de må ikke inneholde sensitiv informasjon.

## Innhold

### [`aja-ca.crt`](http://static.aja.bs.no/aja-ca.crt)

CA-sertifikat som brukes til å verifisere lokalt genererte sertifikater.

Sertifikatet har en varighet på 10 år. For å fornye det, se https://serverfault.com/a/308100/221948

For å sjekke utløpstiden:

    openssl x509 -in ./public/aja-ca.crt -noout -text
